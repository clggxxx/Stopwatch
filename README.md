# Stopwatch
Stopwatch timing
Multi-platform operation is supported


# Use
Click Start, Pause
Double-click to zero

**Record history is not supported at this time**

![Effect](https://raw.githubusercontent.com/KeivnMM/Stopwatch/main/images/stopwatch.png)
